# Frontend Test for Betsson

## About

I have implemented a system to manage a collection of movies. I choosed with minimalistic styles and used bootstrap as a framework. The mock data is implemented in data.service as a in-memory web api. For state management i selected to use rxjs. 

## Installation
```
npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

