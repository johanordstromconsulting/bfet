import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Movie } from './movie';

@Injectable()
export class MovieSearchService {
  constructor(private http: HttpClient) {}

  search(term: string): Observable<Movie[]> {
    return this.http
      .get<Movie[]>(`app/movies/?name=${term}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return observableThrowError(res.error || 'Server error');
  }
}
