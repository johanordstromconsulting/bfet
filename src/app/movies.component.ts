import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Movie } from './movie';
import { MovieService } from './movie.service';

@Component({
  selector: 'fet-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  movies: Movie[] = [];
  genres: string[] = [
    'all genres',
    'action',
    'adventure',
    'biography',
    'comedy',
    'crime',
    'drama',
    'history',
    'mystery',
    'scifi',
    'sport',
    'thriller'
  ];

  constructor(
    private router: Router,
    private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.movieService.getMovies()
      .subscribe(movies => this.movies = movies);
  }

  getMoviesByGenre(genre) {
    if (genre === 'all genres') return this.movies;

    let filteredMovies = this.movies.filter(movie => movie.genres.includes(genre));
    return filteredMovies;
  }

  gotoDetail(movie: Movie): void {
    const link = ['/movie', movie.id];
    this.router.navigate(link);
  }
}
