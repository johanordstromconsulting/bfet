import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Movie } from './movie';
import { MovieService } from './movie.service';

@Component({
  selector: 'fet-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  @Input() movie: Movie;
  @Output() close = new EventEmitter();
  error: any;
  navigated = false; // true if navigated here

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        const id = +params['id'];
        this.navigated = true;
        this.movieService.getMovie(id).subscribe(movie => (this.movie = movie));
      } else {
        this.navigated = false;
        this.movie = new Movie();
      }
    });
  }

  goBack(): void {
    if (this.navigated) {
      window.history.back();
    }
  }
}
