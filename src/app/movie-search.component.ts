import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  switchMap
} from 'rxjs/operators';
import { Movie } from './movie';
import { MovieSearchService } from './movie-search.service';

@Component({
  selector: 'fet-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.scss'],
  providers: [MovieSearchService]
})
export class MovieSearchComponent implements OnInit {
  movies: Observable<Movie[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private movieSearchService: MovieSearchService,
    private router: Router
  ) {}

  search(term: string): void {
    // Push a search term into the observable stream.
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.movies = this.searchTerms.pipe(
      debounceTime(300), // wait for 300ms pause in events
      distinctUntilChanged(), // ignore if next search term is same as previous
      switchMap(
        term =>
        term // switch to new observable each time
        ? // return the http search observable
        this.movieSearchService.search(term)
        : // or the observable of empty movies if no search term
        of<Movie[]>([])
      ),
      catchError(error => {
        // TODO: real error handling
        console.log(`Error in component ... ${error}`);
        return of<Movie[]>([]);
      })
    );
  }

  gotoDetail(movie: Movie): void {
    const link = ['/detail', movie.id];
    this.router.navigate(link);
  }
}
