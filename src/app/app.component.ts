import { Component } from '@angular/core';

@Component({
  selector: 'my-root',
  template: `
     <div class="container">
    <h1>{{title}}</h1>
    <header class="top-bar"></header>
    <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Movie Database';
}
